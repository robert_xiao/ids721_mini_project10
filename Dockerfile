# 使用官方Python镜像作为基础镜像
FROM python:3.8-slim

# 设置工作目录
WORKDIR /app

# 复制当前目录下的所有文件到容器中的/app目录
COPY . /app

# 安装系统依赖
RUN apt-get update && apt-get install -y \
    build-essential \
    libprotobuf-dev \
    protobuf-compiler

# 安装Python依赖
RUN pip install --no-cache-dir flask transformers torch sentencepiece

# 指定对外开放的端口
EXPOSE 5000

# 设置环境变量
ENV HF_HOME=/app/huggingface_cache/


# 运行应用
CMD ["python", "app.py"]
