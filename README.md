# IDS721_mini_project10

## Overview:
Creating a serverless transformer endpoint in Rust involves setting up a serverless function that handles HTTP requests, processes them using the transformer model, and returns a response.<br>
## Requirements:<br>
- Dockerize Hugging Face Rust transformer<br>
- Deploy container to AWS Lambda<br>
- Implement query endpoint<br>
## Steps:
1.	Write the code like the following. The functionality of the code is to translate the input texts in certain languages.<br>
 ![Alt text](image.png)<br>
2.	Write the Dockerfile like the following.<br>
 ![Alt text](image-1.png)<br>
3.	Use the following command code to create a container by docker .<br>
```JavaScript
docker build -t translator_app .<br>
```
4.	Use the following command code to tag the container .<br>
```JavaScript
docker tag translator_app:latest 905418421277.dkr.ecr.us-east-1.amazonaws.com/translator_app:latest
```
5.	Use the following command code to push the container to AWS ECR<br>
```JavaScript
docker push 905418421277.dkr.ecr.us-east-1.amazonaws.com/translator_app:latest
```
The images of translator_app are shown below.<br>
  ![Alt text](image-2.png)<br>
  ![Alt text](image-3.png)<br>
6.	Deploy container to AWS Lambda like the following steps.<br>
i.	Select a container image.<br>
 ![Alt text](image-4.png)<br>
ii.	Create a lambda called Hugging_Face_App.<br>
 ![Alt text](image-5.png)<br>
iii.	Create a API gateway like the following.<br>
 ![Alt text](image-6.png)<br>
7.	Use POSTMAN to send a request to my translator_app, and then output the result like the following.<br>
I post the request to the API gataway URL to get the result of the translator_app like the following. <br>
![Alt text](image-7.png)<br>
 The request is shown below.<br>
 ![Alt text](image-8.png)<br>
The result is shown below.<br>
![Alt text](image-9.png)<br>