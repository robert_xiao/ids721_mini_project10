from flask import Flask, request, jsonify
from transformers import MarianMTModel, MarianTokenizer
# import os
# os.environ["TRANSFORMERS_CACHE"] = "/tmp/huggingface/"

app = Flask(__name__)


model_name = "Helsinki-NLP/opus-mt-en-zh"
tokenizer = MarianTokenizer.from_pretrained(model_name)
model = MarianMTModel.from_pretrained(model_name)

@app.route('/translate', methods=['POST'])
def translate():
    input_text = request.json.get('text', '')
    target_lang = request.json.get('lang', 'zh')

    inputs = tokenizer.encode(input_text, return_tensors="pt", max_length=512, truncation=True)
    outputs = model.generate(inputs, max_length=512)
    translated_text = tokenizer.decode(outputs[0], skip_special_tokens=True)
    return jsonify({"translated_text": translated_text})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
